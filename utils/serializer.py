import pickle


def write_object(out_filename, object):
    file = open(out_filename, 'wb')
    pickle.dump(object, file)
    file.close()