import os.path


class WikipediaDictionary:

    def __init__(self, dictionary_filename):
        self._dictionary_filename = dictionary_filename
        self._dictionary = {}
        self._load_dictionary(dictionary_filename)

    def _load_dictionary(self, dictionary_filename):
        file = None
        try:
            if not os.path.isfile(dictionary_filename):
                return
            file = open(dictionary_filename, encoding='utf-8')
            if not file:
                return
            line = file.readline()
            while line:
                link, source, translation, new_line = line.split(' ')
                source = self._clean_word(source)
                translation = self._clean_word(translation)
                self.add_entry(link, source, translation)
                line = file.readline()
        finally:
            if file:
                file.close()

    def _clean_word(self, translation):
        return translation.replace('_', ' ')

    def _code_word(self, translation):
        return translation.replace(' ', '_')

    def add_entry(self, link, source, target):
        entry = Entry(source, target)
        self._dictionary[link] = entry

    def write_dictionary(self):
        try:
            file = open(self._dictionary_filename, 'w+', encoding='utf-8')
            for link, entry in self._dictionary.items():
                source = self._code_word(entry.get_source())
                target = self._code_word(entry.get_target())
                file.write(link + ' ' + source + ' ' + target + ' \n')
        finally:
            file.close()

    def get_entry(self, link):
        if link in self._dictionary:
            entry = self._dictionary[link]
            return entry.get_source(), entry.get_target()
        return None, None


class Entry:

    def __init__(self, source, target):
        self._source = source
        self._target = target

    def get_source(self):
        return self._source

    def get_target(self):
        return self._target
