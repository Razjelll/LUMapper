import urllib
from urllib.parse import urlparse

import mwclient


class WikipediaHelper:

    def __init__(self, first_langauge, second_language):
        self._first_language = first_langauge
        self._second_language = second_language

        self._first_site = mwclient.Site(first_langauge + '.wikipedia.org')
        self._second_site = mwclient.Site(second_language + '.wikipedia.org')

    def get_title(self, url):
        url = urllib.parse.unquote(url)
        first_language_title = self._get_last_segment(url)
        first_langauge_page = self._first_site.Pages[first_language_title]
        if first_langauge_page.exists:
            lang_lings = first_langauge_page.langlinks()
            for l in lang_lings:
                if l[0] == self._second_language:
                    return l[1]
        return None

    def get_titles(self, url):
        url = urllib.parse.unquote(url)
        first_language_title = self._get_last_segment(url)
        try:
            first_langauge_page = self._first_site.Pages[first_language_title]
        except:
            print('Wystąpił wyjątek ' + url)
            return None, None
        if first_langauge_page.exists:
            lang_links = first_langauge_page.langlinks()
            for l in lang_links:
                if l[0] == self._second_language:
                    return first_language_title, l[1]
        return None, None

    def _get_last_segment(self, url):
        return url.rsplit('/', 1)[-1]
