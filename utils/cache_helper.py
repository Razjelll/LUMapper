import os.path

from data.cacher import Cacher


class CacheHelper:

    def __init__(self):
        self.__cacher = Cacher()

    def cache(self, filename, function, *argument1):
        if os.path.exists(filename):
            print('Ladowanie obiektu')
            object = self.__cacher.load_object(filename)
        else:
            # TODO sprawdzić, czy można to zrobić w jakiś inny sposób
            if not argument1[1]:
                object = function(argument1[0])
            else:
                object = function(*argument1)
            print('Zapisywanie obiektu')
            self.__cacher.write_object(object, filename)
        return object
