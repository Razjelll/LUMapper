from data.models import Relation


def write_relations(filename, relations):
    file = open(filename, 'w+', encoding='utf-8')
    text = '{} {} {} \n'
    for relation in relations:
        target = str(relation.get_target())
        source = str(relation.get_source())
        relation = str(relation.get_relation())
        file.write(text.format(target, source, relation))
    file.close()


def read_relations(filename):
    file = open(filename, encoding='utf-8')

    line = file.readline()
    result = []
    while line:
        target, source, relation, new_line = line.split(' ')
        relation = Relation(int(target), int(source), int(relation))
        result.append(relation)
        line = file.readline()
    file.close()
    return result
