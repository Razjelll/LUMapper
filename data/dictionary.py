class Dictionary:

    # TODO dodać jakąś konfigurację czy coś
    def __init__(self, configuration):
        self._DICTIONARIES_PATH = configuration.dictionaries().get_path()
        self._DICTIONARY_NAME = configuration.dictionaries().get_name()
        self._DICTIONARIES_ORDER = configuration.dictionaries().get_order()
        ''' Cascade dictionary (list of dict).
            Every dictionary is in another position of list.'''
        self._cascade_dictionary = []
        ''' Dictionary for all words (dict of sets)'''
        self._all_dictionary = {}
        # TODO zmienić nazwę
        self._single_translations = {}

        self._load_dictionary()

    def has_single_translation(self, word):
        return word in self._single_translations

    def get_single_translation(self, word):
        return self._single_translations[word]

    def _load_dictionary(self):
        dictionary_format = "{}/{}/{}"
        for dictionary in self._DICTIONARIES_ORDER:
            dictionary_path = dictionary_format.format(self._DICTIONARIES_PATH, dictionary, self._DICTIONARY_NAME)
            file = open(dictionary_path, encoding='utf-8')
            # TODO dodać sprawdzanie czy plik istnieje
            dict = self._parse_dictionary(file)
            self._cascade_dictionary.append(dict)
            # add everything to big dictionary
            for word, translations in dict.items():
                if word not in self._all_dictionary:
                    self._all_dictionary[word] = set()
                for translation in translations:
                    self._all_dictionary[word].add(translation)
            file.close()

        for source_word, translations in self._all_dictionary.items():
            if len(translations) == 1:
                self._single_translations[source_word] = next(iter(translations))

    def _parse_dictionary(self, file):
        result = {}
        for line in file:
            words = line.strip().split("\t")
            source = words[0]
            targets = words[1:]
            if source not in result:
                result[source] = set()
            for target in targets:
                result[source].add(target)
        return result

    def translation_exist(self, word, translation):
        if self.word_exist(word):
            return translation in self._all_dictionary[word]
        return False

    def word_exist(self, word):
        return word in self._all_dictionary

    def find_translation_with_level(self, word):
        level = 1
        for dictionary in self._cascade_dictionary:
            if word in dictionary:
                return dictionary[word], level
            level += 1
        return None, -1

    def find_all_translations(self, word):
        return self._all_dictionary[word]
