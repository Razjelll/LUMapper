class Unit():

    def __init__(self, id, word, variant, pos = None, domain = None, synset=None, definition=None, link=None):
        self._id =id
        self._variant = variant
        self._domain = domain
        self._pos = pos
        self._synset = synset
        self._word = word
        self._definition = definition
        self._link = link
        self._synset_position = 0

    def get_id(self):
        return self._id

    def get_variant(self):
        return self._variant

    def get_domain(self):
        return self._domain

    def get_pos(self):
        return self._pos

    def get_synset(self):
        return self._synset

    def get_word(self):
        return self._word

    def get_definition(self):
        return self._definition

    def get_link(self):
        return self._link

    def get_synset_position(self):
        return self._synset_position

    def set_synset_position(self, position):
        # TODO może być konieczne sprawdzanie, czy jednostka jest polska czy angielska i odpowiednie zmniejszanie pozycji
        self._synset_position = position


class Synset:

    def __init__(self, id):
        self._id = id
        self._units = []
        self._used_units = []

    def add_unit(self, unit):
        self._units.append(unit)

    def add_units(self, units):
        for unit in units:
            self._units.append(unit)

    def get_units(self):
        return self._units

# TODO może być konieczne zmienienie nazwy
class ConnectionSynset:

    def __init__(self, synset):
        self._synset = synset
        self._source_units = set()
        self._remaining_source_units = set()

        self._target_units = {}
        self._remaining_target_units = {}

        self._used_units = {}


    def get_synset(self):
        return self._synset

    def get_source_units(self):
        return self._source_units

    def get_remaining_source_units(self):
        return self._remaining_source_units

    def get_remaining_target_units(self):
        return self._remaining_target_units

    def get_target_units(self):
        return self._target_units

    def get_used_units(self):
        return self._used_units

    def use_unit(self, source_unit, target_unit):
        self._used_units[source_unit] = target_unit
        if source_unit in self._remaining_source_units:
            # del self._remaining_source_units[source_unit]
            self._remaining_source_units.remove(source_unit)
        # self._remaining_target_units.pop(target_unit, None)
        if target_unit.get_id() in self._remaining_target_units:
            del self._remaining_target_units[target_unit.get_id()]
        # self._remaining_target_units.remove(target_unit)

    def add_source_unit(self, unit):
        self._source_units.add(unit)
        self._remaining_source_units.add(unit)

    def add_target_unit(self, unit):
        lemma = unit.get_word()
        if lemma not in self._target_units:
            self._target_units[lemma] = []
        self._target_units[lemma].append(unit)
        # self._target_units[lemma] = unit
        # self._remaining_target_units[lemma] = unit
        self._remaining_target_units[unit.get_id()] = unit

    def get_remaining_target_units(self, lemma = None):
        result = []
        # TODO przyjrzeć się temu
        if not lemma:
            for id, unit in self._remaining_target_units.items():
                result.append(unit)
            return result

        if lemma not in self.get_target_units():
            return result
        units = self.get_target_units()[lemma]
        for unit in units:
            if unit.get_id() in self._remaining_target_units:
                result.append(unit)
        return result

class Result:

    def __init__(self):
        self._result_map = {}

        #TODO może będzie trzeba dodać tutaj jakiś hash czy coś, nie wiem jeszcze co będzie przechowywane w tych mapach
    def add_connection(self, first_unit, second_unit):
        self._result_map[first_unit] = second_unit

class Relation:

    def __init__(self, target, source, relation):
        self._target = target
        self._source = source
        self._relation = relation

    def get_target(self):
        return self._target

    def get_source(self):
        return self._source

    def get_relation(self):
        return self._relation



