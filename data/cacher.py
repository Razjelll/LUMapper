import pickle


class Cacher:

    def __init__(self):
        pass

    def write_object(self, object, filename):
        file = open(filename, 'wb')
        pickle.dump(object, file)
        file.close()

    def load_object(self, filename):
        file = open(filename, 'rb')
        object = pickle.load(file)
        file.close()
        return object
