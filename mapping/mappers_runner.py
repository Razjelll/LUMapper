from mapping.equality_mapper import EqualityMapper
from mapping.feminity_mapper import FeminityMapper
from mapping.single_translation_mapper import SingleTranslationMapper
from mapping.wikipedia_mapper import WikipediaMapper


class MappersRunner:

    def __init__(self, settings, mapper):
        self._modes = settings.get_modes()
        self._mapper = mapper

    def start(self, remove_from_remaining=False, unit_limit=None):
        result = {}
        for mode in self._modes:
            mapper = self._get_mapper(mode)
            mode_result = mapper.run(remove_from_remaining, unit_limit)
            result.update(mode_result)
            # TODO można te wyniki przerzucić jeszcze do mappera
        return result

    def _get_mapper(self, mode_symbol):
        if mode_symbol == 'S':
            return SingleTranslationMapper(self._mapper)
        if mode_symbol == 'W':
            return WikipediaMapper(self._mapper)
        if mode_symbol == 'E':
            return EqualityMapper(self._mapper)
        if mode_symbol == 'Z':
            return FeminityMapper(self._mapper)
