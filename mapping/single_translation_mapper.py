from mapping.base_mapper import BaseMapper


class SingleTranslationMapper(BaseMapper):

    def __init__(self, mapper):
        super(SingleTranslationMapper, self).__init__(mapper)
        self._dictionary = mapper.get_dictionary()
        self._mapper = mapper

    def run_unit(self, unit, synset):
        # TODO dodać to do zasad
        if self._mapper.is_linked(unit):
            return None
        if self._dictionary.has_single_translation(unit.get_word()):
            translation = self._dictionary.get_single_translation(unit.get_word())
            if translation in synset.get_target_units():
                return unit, synset.get_target_units()[translation][0]
        return None

    def _on_finish(self):
        pass
