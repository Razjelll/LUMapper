class BaseMapper:

    def __init__(self, mapper):
        self._mapper = mapper

    def run(self, remove_from_remainig=False, unit_limit=None):
        result = {}
        count = 0
        for id, synset in self._mapper.get_synsets().items():
            # TODO przerobić to na remaining units
            units = synset.get_source_units()
            for unit in units:
                if unit_limit:
                    if count >= unit_limit:
                        return result
                    else:
                        count += 1
                unit_result = self.run_unit(unit, synset)
                if unit_result:
                    source_unit = unit_result[0]
                    target_unit = unit_result[1]
                    result[source_unit] = target_unit
                    if remove_from_remainig:
                        self._mapper.remove_from_remaining_target_unit(target_unit)
                        synset.use_unit(source_unit, target_unit)
        self._on_finish()
        return result

    def run_unit(self, unit, synset):
        raise NotImplementedError("Please Implement this method")

    def _on_finish(self):
        raise NotImplementedError("Please Implement this method")
