import os.path
import os.path

import utils.file_utils as file_utils
from database.old_db_helper import DBHelper
from mapping.base_mapper import BaseMapper


class FeminityMapper(BaseMapper):

    def __init__(self, mapper):
        super(FeminityMapper, self).__init__(mapper)
        self._FEMINITY_RELATION = mapper.get_configuration().relations().get_feminity()
        self._FEMINITY_CACHE = mapper.get_configuration().folders().get_feminity()
        self._FILTERED_CACHE = mapper.get_configuration().folders().get_filtered()
        self._mapper = mapper
        # TODO sprawdzić, czy nie lepiej pobierać helpera z mappera
        self._db_helper = DBHelper(mapper.get_configuration())
        self._feminity_relations = {}
        self._feminity_units = {}
        self._dictionary = mapper.get_dictionary()

        self._load_relations()

    def _load_relations(self):
        if not os.path.exists(self._FEMINITY_CACHE):
            feminity_relations = self._db_helper.get_unit_relations([self._FEMINITY_RELATION])
            file_utils.write_relations(self._FEMINITY_CACHE, feminity_relations)
        else:
            feminity_relations = file_utils.read_relations(self._FEMINITY_CACHE)
        for relation in feminity_relations:
            self._feminity_relations[relation.get_source()] = relation.get_target()
            self._feminity_relations[relation.get_target()] = relation.get_source()

        units_ids = []
        for source, target in self._feminity_relations.items():
            units_ids.append(target)
        self._feminity_units = self._db_helper.get_units(units_ids)

    def run_unit(self, unit, synset):
        if unit.get_id() in self._feminity_relations and self._mapper.is_strong_linked(unit):
            source_unit_id = self._feminity_relations[unit.get_id()]
            target_unit_id = self._mapper.get_strong_linked_unit(unit)
            # TODO może będzie trzeba sprawdzić, czy możliwe jest pobranie jednostki docelowej
            source_unit = self._feminity_units[source_unit_id]
            target_unit = self._mapper.get_target_unit(target_unit_id)
            # TODO przepisać to do reguł
            if source_unit and target_unit and (
                    not self._dictionary.word_exist(source_unit.get_word()) or self._dictionary.translation_exist(
                    source_unit.get_word(), target_unit.get_word())):
                return source_unit, target_unit
        return None

    def _on_finish(self):
        pass


class FeminityRules:

    # TODO uzupełnić reguły
    @staticmethod
    def check_rules(source_unit, target_unit, dictionary):
        return not dictionary.word_exist(source_unit.get_word()) \
               or dictionary.translation_exist(source_unit.get_word(), target_unit.get_word())

    @staticmethod
    def check_rules(source_unit, target_unit, dictionary):
        translations = dictionary.find_all_translations(source_unit.get_word())
        if translations:
            return target_unit.get_word() in translations
        return True
