from data.dictionary import Dictionary
from data.models import Synset, ConnectionSynset
from database.db_helper import DBHelper
from database.old_db_helper import DBHelper as OldDbHelper
from mapping.mappers_runner import MappersRunner
from utils.cache_helper import CacheHelper


class Mapper:

    def __init__(self, configuration, settings):
        self._configuration = configuration
        self._settings = settings
        self._mapping_result = {}
        self._remaining_target_units = {}  # TODO może lepiej bedzie, jakby przerobić to na użyte jednostki
        # TODO chyba będzie to trzeba nazwać jakoś inaczej
        self._synsets = {}
        self._linked_relations = {}
        self._strong_linked = {}
        self._dictionary = None

        self.__cache_helper = CacheHelper()

        self.__setup(configuration, settings)

    def get_settings(self):
        return self._settings

    def __setup(self, configuration, settings):
        self._synsets = self.__cache_helper.cache(configuration.folders().get_synsets()
                                                  , self.__prepare_units_with_possible_units, configuration, settings)
        linked_relations_list = self.__cache_helper.cache(configuration.folders().get_linked(),
                                                          self.__prepare_linked_relations, configuration, None)
        self._linked_relations = linked_relations_list[0]
        self._strong_linked = linked_relations_list[1]
        # self.__prepare_linked_relations(configuration)
        self._fill_remaining_target_units(self._synsets)
        self._dictionary = Dictionary(configuration)

    def __prepare_linked_relations(self, configuration):
        old_db_helper = OldDbHelper(configuration)
        strong_linked = old_db_helper.get_unit_relations([configuration.relations().get_strong()])
        regular_linked = old_db_helper.get_unit_relations([configuration.relations().get_regular()])
        # TODO zobaczyć, czy nie będzie tutaj wielokrotnymch relacji
        linked_relations = {}
        strong_relations = {}
        for link in strong_linked:
            source = link.get_source()
            target = link.get_target()
            linked_relations[source] = target
            strong_relations[source] = target
            strong_relations[target] = source
            # self._linked_relations[link.get_source()] = link.get_target()
            # self._strong_linked[link.get_source()] = link.get_target()
            # self._strong_linked[link.get_target()] = link.get_source()
        for link in regular_linked:
            linked_relations[link.get_source] = link.get_target
            # self._linked_relations[link.get_source()] = link.get_target()
        return [linked_relations, strong_relations]

    def __prepare_units_with_possible_units(self, configuration, settings):
        db_helper = DBHelper(configuration)
        source_units = db_helper.get_lexical_units(settings.get_source_lexicons())
        target_units = db_helper.get_lexical_units(settings.get_target_lexicons())

        source_synsets = self.__prepare_synsets(source_units)
        target_synsets = self.__prepare_synsets(target_units)

        multilingual_relations = db_helper.get_multilingual_relations(
            [configuration.relations().get_multilang_synonym()])
        return self.__get_synset_with_possible_units(source_synsets, target_synsets, multilingual_relations)

    def __prepare_synsets(self, units):
        synsets = {}
        for unit in units:
            synset_id = unit.get_synset()
            if not synset_id in synsets:
                synset = Synset(synset_id)
                synsets[synset_id] = synset
            synsets[synset_id].add_unit(unit)
        return synsets

    def __get_synset_with_possible_units(self, source_synset, target_synset, multilang_relations_map):
        result = {}
        for parent, child in multilang_relations_map.items():
            if child in source_synset:
                synset = source_synset[child]
                if child not in result:
                    connection = ConnectionSynset(synset)
                    # TODO można to przenieść w inne miejsce
                    for unit in synset.get_units():
                        connection.add_source_unit(unit)
                    result[child] = connection
                else:
                    connection = result[child]
                second_synset = target_synset[parent]
                for unit in second_synset.get_units():
                    connection.add_target_unit(unit)
        return result

    def get_configuration(self):
        return self._configuration

    # TODO zmienić nazwę
    def get_synsets(self):
        return self._synsets

    def get_dictionary(self):
        return self._dictionary

    def is_remaining_target_unit(self, target_unit):
        return target_unit.get_id() in self._remaining_target_units

    def get_target_unit(self, unit_id):
        if unit_id in self._remaining_target_units:
            return self._remaining_target_units[unit_id]

    def remove_from_remaining_target_unit(self, target_unit):
        if target_unit.get_id() in self._remaining_target_units:
            del self._remaining_target_units[target_unit.get_id()]

    def _fill_remaining_target_units(self, synsets):
        for id, synset in synsets.items():
            # TODO to jakoś przenieść do klasy modelu
            remaining_units = synset.get_remaining_target_units()
            for unit in remaining_units:
                self._remaining_target_units[unit.get_id()] = unit

    def is_strong_linked(self, unit):
        return unit.get_id() in self._strong_linked

    def get_strong_linked_unit(self, source_unit):
        if self.is_strong_linked(source_unit):
            return self._strong_linked[source_unit.get_id()]

    def is_linked(self, unit):
        return unit.get_id() in self._linked_relations

    def run(self, unit_limit=None):
        print("Start mapping algorith")

        runner = MappersRunner(self._settings, self)
        return runner.start(unit_limit=unit_limit)

    def add_to_result(self, result):
        if result:
            for source_unit, target_unit in result.items():
                self._mapping_result[source_unit] = target_unit

    def _finish(self):
        return self._mapping_result
