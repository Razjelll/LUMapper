from mapping.base_mapper import BaseMapper


class EqualityMapper(BaseMapper):

    def __init__(self, mapper):
        super(EqualityMapper, self).__init__(mapper)
        self._mapper = mapper

    def run_unit(self, unit, synset):
        remaining_units = synset.get_remaining_target_units()
        for target_unit in remaining_units:
            if EqualityRules.check_rules(unit, target_unit):
                return unit, target_unit

    def _on_finish(self):
        pass


class EqualityRules:

    @staticmethod
    def check_rules(source_unit, target_unit):
        # TODO sprawdzić, czy na pewno będzie działało to poprawnie
        FIRST_POSITION = 0
        TARGET_FIRST_POSITION = 0
        source_position = source_unit.get_synset_position()
        target_position = target_unit.get_synset_position()
        return source_unit.get_word() == target_unit.get_word() \
               and (target_position != TARGET_FIRST_POSITION
                    or (source_position == FIRST_POSITION and target_position == TARGET_FIRST_POSITION))
