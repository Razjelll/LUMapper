from mapping.base_mapper import BaseMapper
from utils.wikipedia.wikipedia import WikipediaHelper
from utils.wikipedia.wikipedia_dict import WikipediaDictionary


class WikipediaMapper(BaseMapper):

    def __init__(self, mapper):
        super(WikipediaMapper, self).__init__(mapper)
        SOURCE_LANG = mapper.get_configuration().wikipedia().get_source_lang()
        TARGET_LANG = mapper.get_configuration().wikipedia().get_target_lang()
        DICTIONARY_FOLDER = mapper.get_configuration().folders().get_wikipedia_dictionary()
        self._wikipedia_helper = WikipediaHelper(SOURCE_LANG, TARGET_LANG)
        self._wikipedia_dictionary = WikipediaDictionary(DICTIONARY_FOLDER)
        self._mapper = mapper

    def run_unit(self, unit, synset):
        # TODO ubrać to w jakieś reguły
        if self._mapper.is_linked(unit):
            return None
        if not WikipediaRules.check_domain(unit, self._mapper.get_settings()) or not unit.get_link():
            return None
        link = unit.get_link()
        # TODO można zrobić jakoś tak, aby te jednostki, dla których nie ma odpowiedników w angielskiej wersji też były zapisywane w słowniku
        source_word, target_word = self._wikipedia_dictionary.get_entry(link)
        # if not found in dictionary load from wikipedia site
        if not source_word:
            source_word, target_word = self._wikipedia_helper.get_titles(link)
            # if page contains link to page in another language
            if source_word:
                source_word, target_word = self._repair_units_names(source_word, target_word)
                self._wikipedia_dictionary.add_entry(link, source_word, target_word)
            else:
                return None
        if WikipediaRules.check_rules(source_word, target_word, unit, synset):
            return unit, synset.get_remaining_target_units(target_word)[0]
        return None

    def _repair_units_names(self, source_word, target_word):
        source_word = source_word.replace('_', ' ').lower()
        target_word = target_word.replace('_', ' ').lower()
        return source_word, target_word

    def _on_finish(self):
        self._wikipedia_dictionary.write_dictionary()


class WikipediaRules:

    # TODO można to przerobić w taki sposób
    @staticmethod
    def check_rules(source_word, target_word, source_unit, synset, ):
        if source_word == source_unit.get_word().lower():
            result = synset.get_remaining_target_units(target_word)
            if len(result) == 1:
                return True
        return False

    @staticmethod
    def check_domain(source_unit, settings):
        # 17 - rośliny
        # 24 - substancje
        # 15 - osoby
        # 20 - liczności
        excluded_domain = settings.get_wikipedia_excluded_domains()
        limited_domain = [5, 20, 24]
        # limited_domain = []
        if limited_domain:
            return source_unit.get_domain() in limited_domain
        return not source_unit.get_domain() in excluded_domain
