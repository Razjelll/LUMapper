from database.old_db_helper import DBHelper
from database.old_db_helper import SenseRelation

class RelationRepairer:

    def __init__(self, configuration):
        self._db = DBHelper(configuration)

    def repair(self):
        STRONG_EQUIVALENCE = 3492

        relations = self._db.get_unit_relations([STRONG_EQUIVALENCE])
        in_relations = {}
        out_relations = {}
        for relation in relations:
            in_relations[relation.get_source()] = relation.get_target()
            out_relations[relation.get_target()] = relation.get_source()

        for relation in relations:
            if relation.get_source() in out_relations:
                print("Relacja dwustronna")
                continue
            else:
                newRelation = SenseRelation(relation.get_target(), relation.get_source(), relation.get_relation())
                self._db.insert_relations(newRelation)

