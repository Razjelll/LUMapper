from database.old_db_helper import DBHelper

class Writer:

    def __init__(self, configuration):
        self.__db_helper = DBHelper(configuration)

    def write_to_database(self, links_map, relation):
        self.__db_helper.insert_relations(links_map, relation)
