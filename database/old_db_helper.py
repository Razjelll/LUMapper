import sqlalchemy
from sqlalchemy import Column, Integer, String, Unicode
from sqlalchemy import PrimaryKeyConstraint
from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import and_

from data.models import Relation, Unit


class DBHelper:

    def __init__(self, configuration):
        engine = self._create_alchemy_engine(configuration)
        self._session = self._create_alchemy_session(engine)

    def _create_alchemy_engine(self, configuration):
        host = configuration.old_database().get_host()
        port = configuration.old_database().get_port()
        databse = configuration.old_database().get_database()
        username = configuration.old_database().get_username()
        # TODO można pomyśleć o jakiś kwestiach bezpieczeństwa
        password = configuration.old_database().get_password()
        settings = {'drivername': 'mysql', 'host': host, 'port': port, 'database': databse,
                    'username': username, 'password': password, 'query': {'charset': 'utf8mb4'}}
        url = sqlalchemy.engine.url.URL(**settings)
        engine = sqlalchemy.create_engine(url, echo=False)
        return engine

    def _create_alchemy_session(self, engine):
        session = sqlalchemy.orm.sessionmaker(bind=engine)
        return session()

    def get_unit_relations(self, relations):
        result = []
        query_result = self._query_unit_relations(relations, self._session)
        for row in query_result:
            target = row.target
            source = row.source
            relation = row.relation
            sense_relation = Relation(target, source, relation)
            result.append(sense_relation)
        return result

    def _query_unit_relations(self, relations, session):
        return (session.query(SenseRelation.target, SenseRelation.source, SenseRelation.relation)
                .filter(SenseRelation.relation.in_(relations)))

    def get_units(self, units_ids):
        result = {}
        query_result = self._query_units(units_ids, self._session)
        for row in query_result:
            id = row.id
            lemma = row.lemma
            variant = row.variant
            pos = row.pos
            domain = row.domain
            synset_position = row.position
            project = row.project
            # TODO zrobić to jakoś inaczej, ponieważ może to błędnie obniżyć niektóre pozycje
            if project == -1 and synset_position != 0:
                synset_position -= 1
            unit = Unit(id, lemma, variant, pos, domain)
            unit.set_synset_position(synset_position)
            result[id] = unit
        return result

    def _query_units(self, units_ids, session):
        # TODO można to ograniczyć
        return (
            session.query(LexicalUnit.id, LexicalUnit.lemma, LexicalUnit.domain, LexicalUnit.pos, LexicalUnit.variant,
                          LexicalUnit.project,
                          UnitAndSynset.position)
                .join(UnitAndSynset, UnitAndSynset.unit == LexicalUnit.id)
                .filter(LexicalUnit.id.in_(units_ids)))


    def insert_relations(self, link_map, relation ):
        object_to_save = []
        for source_unit, target_unit in link_map.items():
            if self.get_query(source_unit.get_id(), target_unit.get_id(), relation) != 0:
                print("Duplikat")
                continue
            print(source_unit.get_id())
            senseRelation = SenseRelation()
            # TODO dokładnie sprawdzić, czy to powinno być zrobione w tę stronę
            senseRelation.target = target_unit.get_id()
            senseRelation.source = source_unit.get_id()
            senseRelation.relation = relation
            senseRelation.owner = "Piotr.Anusiewicz"

            try:
                self._session.add(senseRelation)
            except:
                print("Duplication {} {} {}".format(senseRelation.source, senseRelation.target, senseRelation.relation))
        self._session.commit()
            # object_to_save.append(senseRelation)
        # self._session.bulk_save_objects(object_to_save)
        # self._session.commit()

    def insert_relation(self, relation):
        self._session.add(relation)

    def get_query(self, source, target, relation):
        return self._session.query(SenseRelation.source, SenseRelation.target).filter(and_(SenseRelation.source == source, SenseRelation.target == target, SenseRelation.relation==relation)).count()


# database models

Base = declarative_base()


class LexicalUnit(Base):
    __tablename__ = "lexicalunit"

    id = Column('ID', Integer, primary_key=True)
    lemma = Column('lemma', Unicode)
    domain = Column('domain', Integer)
    pos = Column('pos', Integer)
    variant = Column('variant', Integer)
    project = Column('project', Integer)


class SenseRelation(Base):
    __tablename__ = "lexicalrelation"
    __table_args__ = (PrimaryKeyConstraint('PARENT_ID', 'CHILD_ID'),)

    target = Column('PARENT_ID', Integer)
    source = Column('CHILD_ID', Integer)
    relation = Column('REL_ID', Integer)
    owner = Column('owner', String(255))


class RelationType(Base):
    __tablename__ = "relationtype"

    id = Column("ID", Integer, primary_key=True)
    name = Column(String(40))


class UnitAndSynset(Base):
    __tablename__ = "unitandsynset"
    __table_args__ = (PrimaryKeyConstraint("LEX_ID", "SYN_ID"),)

    unit = Column("LEX_ID", Integer)
    synset = Column("SYN_ID", Integer)
    position = Column("unitindex", Integer)
