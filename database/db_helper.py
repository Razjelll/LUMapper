import sqlalchemy
from sqlalchemy import Column, Integer, String, Unicode
from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base

from data.models import Unit


class DBHelper:

    def __init__(self, configuration):
        engine = self._create_alchemy_engine(configuration)
        self._session = self._create_alchemy_session(engine)

    # TODO można zrobić, że sesje otwiera się odzielną metodą, i sprawdza czy wszystko przebiegło poprawnie

    def _create_alchemy_engine(self, configuration):
        host = configuration.database().get_host()
        port = configuration.database().get_port()
        databse = configuration.database().get_database()
        username = configuration.database().get_username()
        # TODO można pomyśleć o jakiś kwestiach bezpieczeństwa
        password = configuration.database().get_password()
        settings = {'drivername': 'mysql', 'host': host, 'port': port, 'database': databse,
                    'username': username, 'password': password, 'query': {'charset': 'utf8mb4'}}
        url = sqlalchemy.engine.url.URL(**settings)
        engine = sqlalchemy.create_engine(url, echo=False)
        return engine

    def _create_alchemy_session(self, engine):
        session = sqlalchemy.orm.sessionmaker(bind=engine)
        return session()

    def get_multilingual_relations(self, relations):
        query_result = self._query_multilingual_relations(relations, self._session)
        result = {}
        for row in query_result:
            parent = row.parent
            child = row.child
            result[parent] = child

        return result

    def _query_multilingual_relations(self, relations, session):
        return (session.query(SynsetRelation.id, SynsetRelation.parent, SynsetRelation.child, SynsetRelation.relation)
                .filter(SynsetRelation.relation.in_(relations)))

    def get_lexical_units(self, lexicons):
        query_result = self._query_lexical_units(lexicons, self._session)
        result = []
        result_set = query_result
        for row in result_set:
            id = row.id
            variant = row.variant
            domain = row.domain
            pos = row.pos
            synset = row.synset
            word = row.word
            definition = row.definition
            link = row.link
            synset_position = row.position
            unit = Unit(id, word, variant, pos, domain, synset, definition, link)
            unit.set_synset_position(synset_position)
            result.append(unit)
            # unit = Unit(row.id, row.variant. row.domain, row.pos, row.synset, row.word)
            # result.append(unit)
        return result

    def _query_lexical_units(self, lexicons, session):
        return (
            session.query(LexicalUnit.id, LexicalUnit.variant, LexicalUnit.domain, LexicalUnit.pos, LexicalUnit.synset,
                          LexicalUnit.position, Word.word, UnitAttribute.definition, UnitAttribute.link)
                .join(Word, LexicalUnit.word_id == Word.id)
                .join(UnitAttribute, LexicalUnit.id == UnitAttribute.id)
                .filter(LexicalUnit.lexicon.in_(lexicons)))


# db models --------------------------------

Base = declarative_base()


class Word(Base):
    __tablename__ = 'word'

    id = Column("id", Integer, primary_key=True)
    word = Column(String(255))


class UnitAttribute(Base):
    __tablename__ = 'sense_attributes'

    id = Column('sense_id', Integer, primary_key=True)
    definition = Column(String(255))
    link = Column(Unicode())


class LexicalUnit(Base):
    __tablename__ = 'sense'

    id = Column('id', Integer, primary_key=True)
    variant = Column("variant", Integer)
    domain = Column("domain_id", Integer)
    pos = Column("part_of_speech_id", Integer)
    synset = Column("synset_id", Integer)
    word_id = Column("word_id", Integer)
    lexicon = Column("lexicon_id", Integer)
    position = Column("synset_position", Integer)


class SynsetRelation(Base):
    __tablename__ = 'synset_relation'

    id = Column('id', Integer, primary_key=True)
    child = Column('child_synset_id', Integer)
    parent = Column('parent_synset_id', Integer)
    relation = Column('synset_relation_type_id', Integer)
