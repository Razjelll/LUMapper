from configparser import ConfigParser


class Settings:

    def __init__(self, settings_filename):
        MODE = 'mode'
        LEXICONS = 'lexicons'
        WIKIPEDIA = 'wikipedia'
        parser = ConfigParser()
        parser.read(settings_filename)
        self.__modes = self._get_list(parser.get(MODE, 'modes'))
        self.__source_lexicons = self._get_list(parser.get(LEXICONS, 'source'))
        self.__target_lexicons = self._get_list(parser.get(LEXICONS, 'target'))
        self.__wikipedia_domains = self._get_list(parser.get(WIKIPEDIA, 'domains'))
        self.__wikipedia_excluded_domains = self._get_list(parser.get(WIKIPEDIA, 'excluded_domains'))

    def _get_list(self, text):
        return text.replace(' ', '').strip().split(',')

    def get_modes(self):
        return self.__modes

    def get_source_lexicons(self):
        return self.__source_lexicons

    def get_target_lexicons(self):
        return self.__target_lexicons

    def get_wikipedia_domains(self):
        return [self.__wikipedia_domains]

    def get_wikipedia_excluded_domains(self):
        return [self.__wikipedia_excluded_domains]
