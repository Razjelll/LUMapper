class EqualityRules:
    '''
        Rules for equality of units.
        1. Units must have equal lemma
    '''

    @staticmethod
    def check_rules(source_unit, target_unit):
        # TODO dorobić tutaj jeszcze zasadę, że jeżeli w źródłowym synsecie pozycja jest równa to w tym drugim też powinna być
        return source_unit.get_word() == target_unit.get_word()


class WikipediaRules:

    # TODO można to przerobić w taki sposób
    @staticmethod
    def check_rules(source_word, target_word, source_unit, synset, ):
        if source_word == source_unit.get_word().lower():
            result = synset.get_remaining_target_units(target_word)
            if len(result) == 1:
                return True
        return False

    @staticmethod
    def check_domain(source_unit):
        # 17 - rośliny
        # 24 - substancje
        # 15 - osoby
        # 20 - liczności
        excluded_domain = [17]
        limited_domain = [20]
        # limited_domain = []
        if limited_domain:
            return source_unit.get_domain() in limited_domain
        return not source_unit.get_domain() in excluded_domain


class FeminityRules:

    @staticmethod
    def check_rules(source_unit, target_unit, dictionary):
        translations = dictionary.find_all_translations(source_unit.get_word())
        if translations:
            return target_unit.get_word() in translations
        return True


class CommonRules:

    def check_linked(self, source_unit, linked_units):
        return source_unit.get_id() not in linked_units
