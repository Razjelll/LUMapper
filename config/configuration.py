from configparser import ConfigParser


class Configuration:

    def __init__(self, configuration_filename):
        DATABASE = 'database'
        OLD_DATABASE = 'old_database'
        parser = ConfigParser()
        parser.read(configuration_filename)
        self._database = DatabaseConfiguration(parser, DATABASE)
        self._old_database = DatabaseConfiguration(parser, OLD_DATABASE)
        self._folders = FoldersConfiguration(parser)
        self._relations = RelationConfiguration(parser)
        self._wikipedia = WikipediaConfiguration(parser)
        self._dictionaries = DictionariesConfiguration(parser)

    def database(self):
        return self._database

    def old_database(self):
        return self._old_database

    def folders(self):
        return self._folders

    def relations(self):
        return self._relations

    def wikipedia(self):
        return self._wikipedia

    def dictionaries(self):
        return self._dictionaries


class DatabaseConfiguration:

    def __init__(self, parser, config_name):
        self._host = parser.get(config_name, 'host')
        self._port = parser.get(config_name, 'port')
        self._database = parser.get(config_name, 'database')
        self._username = parser.get(config_name, 'username')
        self._password = parser.get(config_name, 'password')

    def get_host(self):
        return self._host

    def get_port(self):
        return self._port

    def get_database(self):
        return self._database

    def get_username(self):
        return self._username

    def get_password(self):
        return self._password


class WikipediaConfiguration:

    def __init__(self, parser):
        CONFIG_NAME = 'wikipedia'
        self._source_lang = parser.get(CONFIG_NAME, 'source_lang')
        self._target_lang = parser.get(CONFIG_NAME, 'target_lang')

    def get_source_lang(self):
        return self._source_lang

    def get_target_lang(self):
        return self._target_lang


class FoldersConfiguration:

    def __init__(self, parser):
        CONFIG_NAME = 'folders'
        self._main_folder = parser.get(CONFIG_NAME, 'main_folder')
        self._cache_folder = parser.get(CONFIG_NAME, 'cache_folder')
        self._result_folder = parser.get(CONFIG_NAME, 'result_folder')
        self._result = parser.get(CONFIG_NAME, 'result_file')
        self._wikipedia_dictionary = parser.get(CONFIG_NAME, 'wikipedia_dictionary_file')
        self._filtered = parser.get(CONFIG_NAME, 'filtered_file')
        self._strong_relations = parser.get(CONFIG_NAME, 'strong_relations')
        self._regular_relations = parser.get(CONFIG_NAME, 'regular_relations')
        self._feminity_cache = parser.get(CONFIG_NAME, 'feminity_cache')
        self._synsets_cache = parser.get(CONFIG_NAME, 'synsets_cache')
        self._linked_relations_cache = parser.get(CONFIG_NAME, 'linked_cache')

        self._path_format = '{}/{}/{}'
        self._folder_path_format = "{}/{}"

    def get_cache_folder(self):
        return self._folder_path_format.format(self._main_folder, self._cache_folder)

    def get_result_folder(self):
        return self._folder_path_format.format(self._main_folder, self._result_folder)

    def get_result(self):
        return self._path_format.format(self._main_folder, self._result_folder, self._result)

    def get_wikipedia_dictionary(self):
        return self._path_format.format(self._main_folder, self._cache_folder, self._wikipedia_dictionary)

    def get_filtered(self):
        return self._path_format.format(self._main_folder, self._cache_folder, self._filtered)

    def get_strong_relations(self):
        return self._path_format.format(self._main_folder, self._cache_folder, self._strong_relations)

    def get_regular_relations(self):
        return self._path_format.format(self._main_folder, self._cache_folder, self._regular_relations)

    def get_feminity(self):
        return self._path_format.format(self._main_folder, self._cache_folder, self._feminity_cache)

    def get_synsets(self):
        return self._path_format.format(self._main_folder, self._cache_folder, self._synsets_cache)

    def get_linked(self):
        return self._path_format.format(self._main_folder, self._cache_folder, self._linked_relations_cache)


class RelationConfiguration:

    def __init__(self, parser):
        CONFIG_NAME = 'relations'
        self._strong = parser.get(CONFIG_NAME, 'strong')
        self._regular = parser.get(CONFIG_NAME, 'regular')
        self._weak = parser.get(CONFIG_NAME, 'weak')
        self._feminity = parser.get(CONFIG_NAME, 'feminity')
        self._mulilang_synonym = parser.get(CONFIG_NAME, 'multilang_synonym')

    def get_strong(self):
        return self._strong

    def get_regular(self):
        return self._regular

    def get_weak(self):
        return self._weak

    def get_feminity(self):
        return self._feminity

    def get_multilang_synonym(self):
        return self._mulilang_synonym


class DictionariesConfiguration:

    def __init__(self, parser):
        CONFIG_NAME = 'dictionaries'
        self._path = parser.get(CONFIG_NAME, 'path')
        self._name = parser.get(CONFIG_NAME, 'name')
        self._order = parser.get(CONFIG_NAME, 'order').replace(' ', '').strip().split(',')

    def get_path(self):
        return self._path

    def get_name(self):
        return self._name

    def get_order(self):
        return self._order
