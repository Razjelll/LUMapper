from data.models import Unit

class ResultHelper:

    def __init__(self, configuration):
        self.__RESULT_PATH = configuration.folders().get_result()


    def read(self):
        file = open(self.__RESULT_PATH, encoding='utf-8')

        line = file.readline()

        result = {}
        while line:
            source_unit, target_unit = line.strip().split(":")
            # TODO poprawić to, usunąć space
            # if source_unit[-1] == " ":
            #     source_unit = source_unit[:-1]
            # if target_unit[0] == " ":
            #     target_unit = target_unit[1:]
            try:
                source_id, source_lemma, source_variant = source_unit.split("\t")
                target_id, target_lemma, target_variant = target_unit.split("\t")
            except:
                print()

            source_unit = Unit(source_id, source_lemma, source_variant)
            target_unit = Unit(target_id, target_lemma, target_variant)

            result[source_unit] = target_unit

            line = file.readline()
        return result

    def write(self, result_map):
        file = open(self.__RESULT_PATH, 'w+', encoding='utf-8')
        text = "{}\t{}\t{}:{}\t{}\t{}\n"
        for source, target in result_map.items():
            file.write(text.format(str(source.get_id()), source.get_word(), str(source.get_variant()),
                                   str(target.get_id()), target.get_word(), str(target.get_variant())))
        file.close()